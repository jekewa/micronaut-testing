package micronaut.testing

import io.micronaut.configuration.kafka.annotation.KafkaListener
import io.micronaut.configuration.kafka.annotation.OffsetReset
import io.micronaut.configuration.kafka.annotation.OffsetStrategy
import io.micronaut.configuration.kafka.annotation.Topic

import java.util.concurrent.LinkedBlockingQueue

@KafkaListener(offsetReset = OffsetReset.EARLIEST,
        offsetStrategy = OffsetStrategy.ASYNC_PER_RECORD)
class SimpleKafkaListener {

  def queue = new LinkedBlockingQueue<SimpleBean>()

  @Topic('${kafka.topic-name}')
  void receiveSimpleBean(SimpleBean simpleBean){
    queue.offer(simpleBean)
  }
}
