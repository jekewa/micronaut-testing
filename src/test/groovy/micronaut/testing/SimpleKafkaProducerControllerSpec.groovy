package micronaut.testing

import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.uri.UriTemplate
import spock.lang.Unroll

import java.util.concurrent.TimeUnit

@Unroll
class SimpleKafkaProducerControllerSpec extends BaseSpecification {

  def 'get sends simple bean #description'() {
    given:
    def uri = UriTemplate.of('/simple/create{?id,value}').expand(parameters)
    def get = HttpRequest.GET(uri)

    when:
    def response = httpClient.toBlocking().retrieve(get)

    then:
    def readBean = simpleKafkaListener.queue.poll(60, TimeUnit.SECONDS)

    and:
    response == expected
    readBean.id == parameters.id
    readBean.value == parameters.value

    where:
    description       | parameters                           | expected
    'no parameters'   | [:]                                  | '{"id":null,"value":null}'
    'only id'         | [id: 'new-id']                       | '{"id":"new-id","value":null}'
    'only value'      | [value: 'new-value']                 | '{"id":null,"value":"new-value"}'
    'both parameters' | [id: 'some-id', value: 'some-value'] | '{"id":"some-id","value":"some-value"}'
  }

  def 'get returns api response #description'() {
    given:
    def uri = UriTemplate.of('/simple/{id}{?query-param}').expand(parameters)
    def get = HttpRequest.GET(uri)

    when:
    HttpResponse<String> response = httpClient.toBlocking().exchange(get)

    then:
    1 * mockHttpClient.apiResponse(parameters.id, parameters.queryParam) >> apiResponse

    and:
    response.body() == expectedResponse

    where:
    description      | parameters                             | apiResponse                               | expectedResponse
    'just ID'        | [id: 'id']                             | '{"id":"simple"}'                         | '{"id":"simple","value":null}'
    'with parameter' | [id: 'id', 'query-param': 'parameter'] | '{"id":"simple","value":"has parameter"}' | '{"id":"simple","value":"has parameter"}'
  }

  def 'post sends simple bean #description'() {
    given:
    def post = HttpRequest.POST('/simple', new SimpleBean(parameters))

    when:
    def response = httpClient.toBlocking().retrieve(post)

    then:
    def readBean = simpleKafkaListener.queue.poll(60, TimeUnit.SECONDS)

    and:
    response == expected
    readBean.id == parameters.id
    readBean.value == parameters.value

    where:
    description       | parameters                           | expected
    'no parameters'   | [:]                                  | '{"id":null,"value":null}'
    'only id'         | [id: 'new-id']                       | '{"id":"new-id","value":null}'
    'only value'      | [value: 'new-value']                 | '{"id":null,"value":"new-value"}'
    'both parameters' | [id: 'some-id', value: 'some-value'] | '{"id":"some-id","value":"some-value"}'
  }

  def 'put sends simple bean #description'() {
    given:
    def uri = UriTemplate.of('/simple/{id}').expand([id: id])
    def post = HttpRequest.PUT(uri, new SimpleBean(parameters))

    when:
    def response = httpClient.toBlocking().retrieve(post)

    then:
    def readBean = simpleKafkaListener.queue.poll(60, TimeUnit.SECONDS)

    and:
    response == expected
    readBean.id == id
    readBean.value == parameters.value

    where:
    description       | id  | parameters                           | expected
    'no parameters'   | '1' | [:]                                  | '{"id":"1","value":null}'
    'only id'         | '2' | [id: 'new-id']                       | '{"id":"2","value":null}'
    'only value'      | '3' | [value: 'new-value']                 | '{"id":"3","value":"new-value"}'
    'both parameters' | '4' | [id: 'some-id', value: 'some-value'] | '{"id":"4","value":"some-value"}'
  }

}
