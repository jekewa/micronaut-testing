package micronaut.testing

import io.micronaut.http.HttpRequest
import io.micronaut.http.uri.UriTemplate

import java.util.concurrent.TimeUnit

class SimpleHttpClientControllerSpec extends BaseSpecification {

  def 'get sends simple bean #description'() {
    given:
    def uri = UriTemplate.of('/client/{?type,paras}').expand(parameters)
    def get = HttpRequest.GET(uri)

    when:
    def response = httpClient.toBlocking().retrieve(get)

    then:
    def readBean = simpleKafkaListener.queue.poll(60, TimeUnit.SECONDS)

    and:
    response == expected
    readBean.id == parameters.id
    readBean.value == parameters.value

    where:
    description           | parameters                           | expected
    'no parameters'       | [:]                                  | '{"id":null,"value":null}'
    'all-meat'            | [type: 'all-meat']                   | '{"id":"new-id","value":null}'
    'neat-and-filler'     | [type: 'all-meat']                   | '{"id":"new-id","value":null}'
    'something different' | [type: 'all-meat']                   | '{"id":"new-id","value":null}'
    'one paras'           | [paras: '1']                         | '{"id":null,"value":"new-value"}'
    'two paras'           | [paras: '1']                         | '{"id":null,"value":"new-value"}'
    'alpha paras'         | [paras: '1']                         | '{"id":null,"value":"new-value"}'
    'both parameters'     | [id: 'some-id', value: 'some-value'] | '{"id":"some-id","value":"some-value"}'
  }

}
