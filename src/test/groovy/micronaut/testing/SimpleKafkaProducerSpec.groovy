package micronaut.testing


import java.util.concurrent.TimeUnit

class SimpleKafkaProducerSpec extends BaseSpecification {

  def 'sends simple bean'() {
    given:
    def simpleBean = new SimpleBean(id: 'id', value: 'value')

    when:
    simpleKafkaProducer.sendSimpleBean(simpleBean)

    then:
    def readBean = simpleKafkaListener.queue.poll(60, TimeUnit.SECONDS)

    and:
    simpleBean.id == readBean.id
    simpleBean.value == readBean.value
  }
}
