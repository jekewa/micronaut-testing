package micronaut.testing

import io.micronaut.http.HttpRequest

class SimpleStaticControllerSpec extends BaseSpecification {
  def 'gets hello world'() {
    when:
    def response = httpClient.toBlocking().retrieve(HttpRequest.GET('/'))

    then:
    response == 'Hello, world!'
  }
}
