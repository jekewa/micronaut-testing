package micronaut.testing

import io.micronaut.configuration.kafka.config.AbstractKafkaConfiguration
import io.micronaut.context.ApplicationContext
import io.micronaut.http.client.HttpClient
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.annotation.MicronautTest
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

@MicronautTest
class BaseSpecification extends Specification {

  @Shared
  @AutoCleanup
  ApplicationContext context

  @Shared
  EmbeddedServer embeddedServer

  @Shared
  SimpleKafkaProducer simpleKafkaProducer

  @Shared
  SimpleKafkaListener simpleKafkaListener

  @Shared
  HttpClient httpClient

  @Shared
  SimpleHttpClient mockHttpClient = Mock()

  def setupSpec() {

    embeddedServer = ApplicationContext.build(['kafka.bootstrap.servers'                   : 'localhost:${random.port}',
                                               (AbstractKafkaConfiguration.EMBEDDED)       : true,
                                               (AbstractKafkaConfiguration.EMBEDDED_TOPICS): ['${kafka.topic-name}']])
            .build()
            .registerSingleton(SimpleHttpClient, mockHttpClient)
            .start()
            .getBean(EmbeddedServer)
            .start()

    context = embeddedServer.applicationContext

    httpClient = context.createBean(HttpClient, embeddedServer.getURL())

    simpleKafkaListener = context.getBean(SimpleKafkaListener)
    simpleKafkaProducer = context.getBean(SimpleKafkaProducer)
  }

}
