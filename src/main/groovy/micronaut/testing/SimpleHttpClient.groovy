package micronaut.testing

import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.QueryValue
import io.micronaut.http.client.annotation.Client
import io.reactivex.Flowable
import io.reactivex.Single

import javax.annotation.Nullable

@Client('simple-http-client')
interface SimpleHttpClient {

  @Get
  Single<List<String>> apiResponse(@QueryValue('type')String type, @QueryValue('paras')String paras)
}
