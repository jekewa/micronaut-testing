package micronaut.testing

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get

@Controller('/')
class SimpleStaticController {

  @Get(produces = MediaType.TEXT_PLAIN)
  @SuppressWarnings("GrMethodMayBeStatic")
  String helloWorld() {
    return 'Hello, world!'
  }
}
