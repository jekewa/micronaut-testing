package micronaut.testing

import com.fasterxml.jackson.databind.ObjectMapper
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.QueryValue

import javax.annotation.Nullable
import javax.inject.Inject

@Controller('/client')
class SimpleHttpClientController {

  @Inject
  SimpleHttpClient simpleHttpClient

  def objectMapper = new ObjectMapper()

  def allowedTypes = ['all-meat', 'meat-with-filler']

  @Get('/bacon{?paras,type}')
  HttpResponse getBacon(@QueryValue('type') @Nullable String type, @QueryValue('paras') @Nullable String paras) {

    def requestType = allowedTypes.find { it == type } ?: allowedTypes.first()
    def requestParas = paras =~ /\d*/ ?: 1

    def response = simpleHttpClient.apiResponse(requestType, requestParas as String).blockingGet()

    if (!response)
      return HttpResponse.notFound(objectMapper.writeValueAsString([type: type, paras: paras]))

    return HttpResponse.ok(objectMapper.writeValueAsString(response))
  }
}
