package micronaut.testing

import io.micronaut.configuration.kafka.annotation.KafkaClient
import io.micronaut.configuration.kafka.annotation.Topic

@KafkaClient(acks = KafkaClient.Acknowledge.ONE)
interface SimpleKafkaProducer {

  @Topic('${kafka.topic-name}')
  void sendSimpleBean(SimpleBean)
}
