package micronaut.testing

import com.fasterxml.jackson.databind.ObjectMapper
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.*

import javax.annotation.Nullable
import javax.inject.Inject

@Controller('/kafka')
class SimpleKafkaProducerController {

  @Inject
  SimpleKafkaProducer simpleKafkaProducer

  def objectMapper = new ObjectMapper()

  @Get('/create{?id,value}')
  HttpResponse submitBean(@QueryValue('id') @Nullable String id, @QueryValue('value') @Nullable String value) {

    if (!id || !value) {
      return HttpResponse.badRequest()
    }

    def simpleBean = new SimpleBean(id: id, value: value)

    simpleKafkaProducer.sendSimpleBean(simpleBean)

    return HttpResponse.ok(objectMapper.writeValueAsString(simpleBean))
  }

  @Post
  HttpResponse submitBean(@Body SimpleBean simpleBean) {
    if (!simpleBean.id || !simpleBean.value) {
      return HttpResponse.badRequest()
    }

    simpleKafkaProducer.sendSimpleBean(simpleBean)

    return HttpResponse.created(objectMapper.writeValueAsString(simpleBean))
  }

  @Put('/{id}')
  String submitBean(@PathVariable('id') String id, @Body SimpleBean simpleBean) {
    def newBean = new SimpleBean(id: id, value: simpleBean.value)
    if (!newBean.id || !newBean.value) {
      return HttpResponse.badRequest()
    }

    simpleKafkaProducer.sendSimpleBean(newBean)

    return objectMapper.writeValueAsString(newBean)
  }
}
